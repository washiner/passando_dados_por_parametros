import 'package:flutter/material.dart';
import 'package:parametros_aula1/tela_usuario.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController nome = TextEditingController();

  bool checkValor = false;

  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Home"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FlutterLogo(
              size: 120,
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: TextField(
                controller: nome,
                decoration: InputDecoration(labelText: "Digite seu nome"),
              ),
            ),
            RaisedButton(
                color: Colors.black,
                child: Text(
                  "Salvar",
                  style: TextStyle(fontSize: 22, color: Colors.white),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => TelaUsuario(
                                nomedigitado: nome.text,
                              )));
                })
          ],
        ),
      ),
    );
  }
}
